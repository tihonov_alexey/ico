
//================================================================//
//*********** projects.js ***********//
//*********** Логика работы с проектами
//*********** Поиск по проектам
//*********** Создание нового проекта
//*********** Удаление проекта
//*********** Редактирование проекта
//*********** Публикация проекта и снятие с публикации
//================================================================//

$(document).ready(function(){

    var errorTopMargin = 50;

    //================================================================//
    //*********** Создание проекта по запросу ***********//
    //================================================================//

    $('#search-project').on('keyup', function(e){

        if ($(this).val().length > 1){

            //------------ данные для ajax-запроса
            var data = {};
            data.key = $(this).val();

            
            data.action = 'searchproject';
            data.catid = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);

            //------------ ajax-запрос на поиск проекта

            $.ajax({
                url: "/admin/projects",
                type: 'POST',
                dataType: 'json',
                data: data,
                error: function(){
                    showError('Ошибка базы данных!', errorTopMargin);
                }
            }).done(function(data){

                //------------ data.result - ответ от сервера (найденный проект)

                $('.projects').children().remove();
                $('.projects').append(data.result);

                //------------ если ничего не найдено по запросу
                if (data.result.length == 0){
                    $('.projects').append('<div class="notfound">Ой... Похоже ничего не найдено!</div>')
                }

            });
            //------------ }
        }
    });

    //------------------ функция для подстановки тегов

    if ($('#tags').length){

        var tagInput = $('#tags'); // элемент для ввода тега
        var tagAutoFillBar = $('.autofill-bar'); // панель для вывода результатов подстановки
        var tagAutoFillClass = 'autofill-bar'; // класс панели для вывода результатов подстановки (String)
        var tagContainer = $('#project-tags'); // контейнер для выгрузки выбранных тегов
        var tagItemClass = 't-item'; // класс элемента с тегом в контейнере
        var routeToTagSearch = '/admin/project'; // роут для ajax-запроса к серверу (совпадение тегов для постановки)

        tagMeBabe(tagInput, tagAutoFillBar, tagAutoFillClass, tagContainer, tagItemClass, routeToTagSearch);
    }


    //================================================================//
    //*********** Сохранение проекта ***********//
    //================================================================//

    function saveProject(type, button){

        //--------------- button - кнопка, с которой был переход
        //--------------- type - новый проект или редактирование существующей

        if (!button.hasClass('working')){

            //------------ }

            //--------------- проверка выбрана ли хоть одна категория
            if ($('#project-categories .active').length > 0){

                //--------------- проверка на обязательные поля
                if ($('#title').val().length == 0){
                    showError('Введите название проекта!', errorTopMargin);
                } else if ($('#alias').val().length == 0) {
                    showError('Введите URL проекта!', errorTopMargin);
                } else {
                    console.log(123);
                    button.addClass('working');

                    //------------ данные для ajax-запроса
                    var data = {};

                    //--------------- передать тип на сервер!
                    data.action = type;

                    
                    //------------ если это edit, нужно передать id
                    if (type == 'editproject'){
                        data.id = $('.id-info').attr('id');
                    }
                    //------------ }

                    data.title = $('#title').val();
                    data.alias = $('#alias').val();
                    data.moderator = $('#moderator').text();
                    data.days = $('#days').val();

                    data.parent = $('.id-info').attr('data-parent');
                    //------------ }

                    var catArray = [];
                    $('#project-categories .active').each(function(){
                        //------------ собираем id с категорий
                        catArray.push($(this).attr('id'));
                        //------------ }
                    });
                    data.categories = catArray;

                    var tagArray = [];
                    $('#project-tags .t-item').each(function(){
                        tagArray.push($(this).text());
                    });
                    data.tags = tagArray;

                    data.description = mediaElement.saveDescription();

                    $('.loader').fadeIn('fast');

                    $.ajax({
                        url: '/admin/project/',
                        type: 'POST',
                        dataType: 'json',
                        data: data,
                        error: function(data){
                            showError(data.responseText, errorTopMargin);
                            button.removeClass('working');
                            $('.loader').fadeOut('fast');
                        }
                    }).done(function(data){

                        var projectid = data.id;

                        if (globalProjectMainImageFileNameToUpload){
                            var fd = new FormData();

                            fd.append('fileUpload', globalProjectMainImageFileNameToUpload);
                            fd.append('action', 'savemainimage');
                            fd.append('id',  projectid);

                            var xhr = new XMLHttpRequest();
                            xhr.open('post', '/admin/project/', true);
                            xhr.send(fd);

                            xhr.onreadystatechange = function() {
                                if (this.readyState == 4) {
                                    if (xhr.responseText != 'Success'){
                                        showError(xhr.responseText, errorTopMargin);
                                        button.removeClass('working');
                                        $('.loader').fadeOut('fast');
                                    } else {
                                        afterSave();
                                    }
                                }
                            }
                        } else {
                            afterSave();
                        }
                    });

                    function afterSave() {

                        button.removeClass('working');
                        $('.loader').fadeOut('fast');
                        var href = window.location.href;
                        href = href.substr(0, href.lastIndexOf('/'));
                        href = href.replace('project', 'projects');
                        window.location.href = href;
                    }
                    //------------ }
                }

            } else {
                showError('Выберите хотя бы одну категорию!', errorTopMargin);
            }
        }
    }

    //================================================================//
    //*********** Редактирование проекта ***********//
    //================================================================//

    $('#edit-project').on('click', function(){

        var button = $(this);

        //----------- проверяем нужно ли удалить старое изображение
        if (button.hasClass('todelete')){

            var data = {};

            data.id = $('.id-info').attr('id');
            data.action = 'deletemainimg';
            data.file = '..' + button.attr('data-src');

            $.ajax({
                url: '/admin/project/',
                type: 'POST',
                dataType: 'json',
                data: data,
                error: function(data){
                    showError(data.responseText, errorTopMargin);
                }
            }).done(function(){
                saveProject('editproject', button);
            });
            //------------ }
        } else {
            saveProject('editproject', button);
        }

    });

    //================================================================//
    //*********** Новый проект ***********//
    //================================================================//

    $('#new-project').on('click', function(){

        var button = $(this);
        saveProject('newproject', button);

    });

    //================================================================//
    //*********** Удаление проекта ***********//
    //================================================================//

    $('#delete-project').on('click', function(){

        $('body').append('<div class="popup-holder dynamic">' +
            '<div class="popup-content" style="height: 200px; width: 600px; margin-left: -300px; margin-top: -100px;">' +
            '<div class="popup-header">Удаление проекта</div>' +
            '<div class="full">Вы действительно хотите удалить проект ' + $('#title').val() +'?' +
            '</div>' +
            '<div class="clear centered">' +
            '<div class="button green" id="yes-delete-project">Удалить</div>' +
            '<div class="button grey p-cancel">Отмена</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

        $('.popup-holder.dynamic').fadeIn();

    });

    $('body').on('click', '#yes-delete-project', function(){

        var button = $(this);

        if (!button.hasClass('working')){

            button.addClass('working');

            var data = {};
            data.action = 'deleteproject';
            data.id = $('.id-info').attr('id');

            $.ajax({
                url: "/admin/project",
                type: 'POST',
                dataType: 'json',
                data: data,
                error: function(){
                    showError('Невозможно удалить проект!', errorTopMargin);
                    button.removeClass('working');
                }
            }).done(function(data){

                window.location.href = "/admin/categories";
                var href = window.location.href;
                href = href.substr(0, href.lastIndexOf('/'));

                $('.id-info').attr('id', 'deleted');
                href = "/admin/categories";

                window.location.href = href;
            });
            //------------ }
        }
    });

    //================================================================//
    //*********** Публикация проекта ***********//
    //================================================================//

    $('.projects').on('click', '.published span', function(){

        var button = $(this);

        //------------ данные для ajax-запроса
        var data = {};
        var flag = 'yes';
        if (button.hasClass('yes')){
            flag = 'no';
        }
        data.flag = flag;

        data.id = button.parent().parent().attr('id');
        data.action = 'setpublished';

        $.ajax({
            url: '/admin/projects/',
            type: 'POST',
            dataType: 'json',
            data: data,
            error: function(){
                showError('Ошибка базы данных', errorTopMargin);
            }
        }).done(function(){

            if (flag == 'yes'){
                button.removeClass('no').addClass('yes').text('Опубликован');
                showSuccess('Проект опубликован', errorTopMargin);
            } else {
                button.removeClass('yes').addClass('no').text('Не опубликован');
                showSuccess('Проект снят с публикации', errorTopMargin);
            }
        });
        //------------ }
    });
});
