const contract = require('truffle-contract');
const token_artifact = require('../contract/build/contracts/icoToken.json');
const crowdsale_artifact = require('../contract/build/contracts/icoCrowdsale.json');

const Token = contract(token_artifact);
const CrowdSale = contract(crowdsale_artifact);
const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider("http://localhost:8545");
const web3 = new Web3(provider);
web3.eth.defaultAccount = web3.eth.accounts[0];
// CrowdSale.setProvider(provider);
// Token.setProvider(provider);


module.exports = {
    getTokenBalance: function(account, callback) {
        Token.deployed().then(function(instance){
            return instance.balanceOf.call(account, {from: account});
        }).then(function(balance){
            console.log(balance);
            callback(balance);
        }).catch(function(e) {
            console.log(e);
            callback("Error getTokenBalance");
        });
    },
    getTokenAddress: function(callback) {
        Token.deployed().then(function(instance){
            return instance.address;
        }).then(function(address){
            callback(address);
        }).catch(function(e) {
            console.log(e);
            callback("Error getTokenAddress");
        });
    },
    getTokenInstance: function(callback) {
        Token.deployed().then(function(instance){
            return instance;
        }).then(function(instance){
            callback(instance);
        }).catch(function(e) {
            console.log(e);
            callback("Error getTokenInstance");
        });
    },
    buyToken: function(purchaser, value, callback) {
        CrowdSale.deployed().then(function(instance) {
            return instance;
        }).then(function(instance){
            var c = instance;
            Token.deployed().then(function(t){
                var purchaser = web3.eth.accounts[2];
                var tokenAddress = t.address;
                console.log(tokenAddress);
                var tr = web3.eth.sendTransaction({from: purchaser, to: c.address, value: web3.toWei(value, 'ether'), gas: 900000})
                callback(tr);
            }).catch(function(e) {
                console.log(e);
                callback("Error sendTransaction");
            });
        }).catch(function(e) {
            console.log(e);
            callback("Error crowdSale");
        });
    }
};