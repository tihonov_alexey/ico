
var log = require('./libs/log')(module);
var Admins = require('./models/admins').Admin;
var Categories = require('./models/categories').Category;
var Myconfig = require('./libs/myconfig');
var async = require('async');

exports.commonMiddleware = function (req, res, next){

    //====================================================================================//
    //******************* Параллельный запуск функций ********************//
    //====================================================================================//

    async.parallel([
        getEnvironment,
        getUserRights,
        getCurrentDate,
        getCategories
    ], function(err){
        if (err){
            log.error('------ Error ------ ' + err);
            next();
        } else {
            next();
        }
    });

    //====================================================================================//
    //******************* Переменные res.locals ********************//
    //====================================================================================//
    function getEnvironment(callback){

        var originalPageUrl = req.originalUrl;

        if (originalPageUrl.indexOf('?') != -1){
            originalPageUrl = originalPageUrl.substr(0, originalPageUrl.indexOf('?'));
        }
        res.locals.pageurl = req.protocol + '://' + req.get('host') + originalPageUrl;
        res.locals.currenthost = req.headers.host;
        res.locals.companyname = Myconfig.companyname;
        res.locals.pagenoindex = 'no';


        res.locals.urltail = '';
        res.locals.locals = Myconfig.locals;

        res.locals.metatitle = '';
        res.locals.metadescription = '';
        res.locals.metakeywords = '';
        res.locals.socialimage = Myconfig.images.socialimage;
        res.locals.noimage = Myconfig.images.noimage;

        res.locals.page = '';
        res.locals.tags = [];
        res.locals.pagination = null;

        res.locals.env = process.env.NODE_ENV;

        callback();
    }

    //====================================================================================//
    //******************* Проверка прав администратора ********************//
    //====================================================================================//
    function getUserRights(callback){

        if (req.session.user){

            Admins.findOne({_id: req.session.user}, function(err, user){

                if (user){

                    res.locals.adminrights = user.rights;
                    res.locals.adminname = user.username;
                    res.locals.address = user.address;

                } else {

                    res.locals.adminrights = '';
                    res.locals.adminname = '';
                    res.locals.address = '';
                }

                callback();
            })

        } else {

            res.locals.adminrights = '';
            res.locals.adminname = '';
            res.locals.address = '';
            callback();
        }
    }


    //====================================================================================//
    //******************* Переменные для текущего месяца и года ********************//
    //====================================================================================//
    function getCurrentDate(callback){

        var date = new Date();
        var month = date.getMonth();
        month = Myconfig.locals.month[month];

        res.locals.currentdate = month;
        res.locals.fullyear = date.getFullYear();
        callback();
    }

    //====================================================================================//
    //******************* Сбор категорий для основного меню блога ********************//
    //====================================================================================//
    function getCategories(callback) {

        Categories.find({ismain: false}).sort({position: 1}).exec(function(err, categories){
            res.locals.categories = categories;
            res.locals.category = null;
            callback();
        })
    }

};

