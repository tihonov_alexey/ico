
module.exports = function(app){

//=====================================================//
//************ Admin ************//
//=====================================================//

    var startAdminRoute = require('./admin/admin');
    app.get('/admin', startAdminRoute.get);
    var joinRoute = require('./admin/join');
    app.get('/join', joinRoute.get);
    var loginRoute = require('./admin/login');
    app.post('/login', loginRoute.post);
    var logoutRoute = require('./admin/logout');
    app.get('/admin/logout', logoutRoute.get);

    //====================== Admin ==========================//
    var accountRoute = require('./admin/account');
    app.get('/admin/account', accountRoute.get);

    //====================== Categories ==========================//
    var categoriesListRoute = require('./admin/categories');
    app.get('/admin/categories', categoriesListRoute.get);

    var editCategoryRoute = require('./admin/editcategory');
    app.post('/admin/categories', editCategoryRoute.post);

    app.get('/admin/editcategory/:id', editCategoryRoute.get);


    //====================== Projects ==========================//
    var projectsListRoute = require('./admin/projects');
    app.get('/admin/projects/:id', projectsListRoute.get);
    app.post('/admin/projects', projectsListRoute.post);

    var editProjectRoute = require('./admin/editproject');
    app.get('/admin/project/:id/:label', editProjectRoute.get);
    app.post('/admin/project', editProjectRoute.post);

    //====================== Projects ==========================//
    var myprojectsListRoute = require('./admin/myprojects');
    app.get('/admin/myprojects', myprojectsListRoute.get);

//=====================================================//
//************ Client ************//
//=====================================================//

    //====================== Categories ==========================//
    var categoryRoute = require('./client/category');
    app.get('/', categoryRoute.get);
    app.get('/:alias', categoryRoute.get);

    app.post('/', categoryRoute.post);

    //====================== Projects ==========================//
    var projectRoute = require('./client/project');
    app.get('/project/:alias', projectRoute.get);

    //====================== Tags ==========================//
    var tagRoute = require('./client/tag');
    app.get('/projects/:tag', tagRoute.get);
    app.post('/tag', tagRoute.post);

};
