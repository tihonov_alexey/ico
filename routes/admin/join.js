
var log = require('../../libs/log')(module);
var Admin = require('../../models/admins').Admin;

exports.get = function(req, res){

    res.locals.title = "Регистрация";
    res.locals.page = "join";

    if (req.session.user){

        Admin.findOne({_id:req.session.user}, function(err){
            if (err){
                res.render('./admin/join/join');
            } else {
                res.redirect('/admin/categories');
            }
        })
    } else {
        res.render('./admin/join/join');
    }

};