
var log = require('../../libs/log')(module);
var Categories = require('../../models/categories').Category;
var Projects = require('../../models/projects').Project;

exports.get = function(req, res){

    if (req.session.user) {

        res.locals.page = 'categories.ejs';
        res.locals.title = 'Категории';

        var indexArray = [];

        function getProjectsCount(category, callback){

            Projects.count({categories: (category._id).toString()}, function (err, count) {
                var projectCount = 0;
                if (count){
                    projectCount = count;
                }
                category.projectCount = projectCount;
                var pos = indexArray.indexOf(category.id);

                callback(category, pos);
            });
        }

        Categories.find().sort({position: 1}).exec(function(err, categories){
            if (categories && (categories.length > 0)){

                var catArray = [];
                var counter = 0;

                for (var i=0; i<categories.length; i++){

                    indexArray.push(categories[i].id);

                    getProjectsCount(categories[i], function(category, pos){
                        if (category){
                            catArray[pos] = category;
                        }
                        counter++;
                        if (counter == categories.length){
                            res.locals.categories = catArray;
                            res.render('./admin/categories/categories');
                        }
                    });
                }
            } else {

                res.locals.categories = [];
                res.render('./admin/categories/categories');
            }
        });

    } else {
        res.render('./admin/login/login');
    }

};
