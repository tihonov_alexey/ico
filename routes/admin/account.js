var crypto = require('crypto');
var log = require('../../libs/log')(module);
var Admin = require('../../models/admins').Admin;
var Config = require('../../libs/myconfig');
var Web3 = require('web3');
var contract = require("truffle-contract");
// var truffle_connect = require('../../connection/app.js');
var provider = new Web3.providers.HttpProvider("http://localhost:8545");
const fs = require('fs');
web3.setProvider(provider);
var t_source = fs.readFileSync("/home/tixon/PhpstormProjects/app/contract/build/contracts/icoToken.json");
var c_source = fs.readFileSync("/home/tixon/PhpstormProjects/app/contract/build/contracts/icoCrowdsale.json");

exports.get = function(req, res){

    function getBalance(account, callback){
         balance = web3.eth.getBalance(account);
         balance = web3.fromWei(web3.toDecimal(balance), "ether" );
         callback(balance);
         return balance;
    }

    /*function getTokenBalance(account, callback){
        truffle_connect.getTokenBalance(account, function (balance) {
            tokenBalance = web3.fromWei(balance, "ether");
            console.log(tokenBalance);
            res.locals.tokenBalance = tokenBalance;
            callback(tokenBalance);
            return tokenBalance;
        });
    }

    function buyToken(account, value){
        truffle_connect.buyToken(account, value,function (result) {
            console.log(result);
        });
    }*/


    async function dapp(t, c) {
        const token = await tokenContract.at(t);
        const crowdsale = await CrowdSaleContract.at(c);
        console.log('crowdsale address: ' + crowdsale.address);
        web3.eth.defaultAccount = web3.eth.accounts[0];
        var purchaser = web3.eth.accounts[1];
        if (crowdsale) {
            // token.addMinter(crowdsale.address);
            balance = token.balanceOf.call(purchaser, {from: purchaser});
            balance = web3.fromWei(web3.toDecimal(balance), "ether");
            console.log(balance);
            console.log(web3.toDecimal(crowdsale.openingTime()));
            console.log(web3.toDecimal(crowdsale.closingTime()));
            // web3.eth.sendTransaction({from: purchaser, to: crowdsale.address, value: web3.toWei(5, 'ether'), gas: 900000});
            balance = token.balanceOf.call(purchaser, {from: purchaser});
            balance = web3.fromWei(web3.toDecimal(balance), "ether");
            console.log(balance);
        }
    }

    async function deployToken() {
        // Deploy ICO Token contract instance
        deployedContract = await tokenContract.new({
            data: token_bytecode,
            from: account,
            gas: 3000000
        }, (err, res) => {
            if (err) {
                console.log(err);
                return;
            }

            // If we have an address property, the contract was deployed
            if (res.address) {
                console.log('Token address: ' + res.address);
                testContract(res.address);
                deployContract(res);
            }

            function testContract(address) {
                // Reference to the deployed contract
                const token = tokenContract.at(address);

                // Assert initial account balance, should be 100000
                var balance = token.balanceOf.call(account, {from: account});
                balance = web3.fromWei(web3.toDecimal(balance), "ether" );
                console.log(balance);
            }
        });
    }

    async function deployContract(token) {
        console.log("Deploying the ICO CrowdSale contract");
        // Deploy ICO CrowdSale contract instance
        const openingTime = web3.eth.getBlock('latest').timestamp + 60; // 60 secs in the future
        const closingTime = openingTime + 86400 * 20; // 20 days
        const rate = new web3.BigNumber(1);
        const wallet = web3.eth.accounts[1];
        cContract = CrowdSaleContract.new(openingTime, closingTime, rate, wallet, token.address, {
            data: c_bytecode,
            from: account,
            gas: 3000000
        }, (err, res) => {
            if (res.address) {
                console.log('CrowdSale Contract address: ' + res.address);
            }
        });
    }

    if (req.session.user) {

        res.locals.page = 'account.ejs';
        res.locals.title = 'Личный кабинет';

        web3 = new Web3(provider);
        if (web3.isConnected()) {
            var icoToken = JSON.parse(t_source);
            var icoCrowdSale = JSON.parse(c_source);
            var token_abi = icoToken.abi;
            var c_abi = icoCrowdSale.abi;
            var token_bytecode = icoToken.bytecode;
            var c_bytecode = icoCrowdSale.bytecode;
            // Contract object
            var tokenContract = web3.eth.contract(token_abi);
            var CrowdSaleContract = web3.eth.contract(c_abi);
            var account = web3.eth.accounts[0];
            var balance = web3.eth.getBalance(account);
            balance = web3.fromWei(web3.toDecimal(balance), "ether" );
            console.log(balance);
            console.log("Deploying the ICO Token contract");
            const t = "0x69c91c3af622d2379255c7925f6e6e6e4238803f";
            const c = "0x8545cde779a6f11fd5f3fde2121fd2a23a681eda";

            dapp(t, c);

            // deployToken();


            res.locals.address = web3.eth.accounts[1];
            res.locals.balance = getBalance(res.locals.address, function(balance){});
            if (res.locals.balance > 0) {
                res.locals.tokenBalance = 0;
                res.render('./admin/account/account');
                /*res.locals.tokenBalance = getTokenBalance(res.locals.address, function(tokenBalance){
                    // buyToken(res.locals.address, 1);
                    res.render('./admin/account/account');
                });*/
            } else {
                res.render('./admin/categories/categories');
            }

            /*console.log("**** GET /getTokenInstance****");
            truffle_connect.getTokenInstance(function (instance) {
                token = instance;
                if (token) {
                    console.log("**** GET /crowdSale ****");
                    truffle_connect.buyToken(function (answer) {
                        console.log(answer);
                    });
                }
                token.symbol().then(function(tokenSymbol) {
                    console.log("Token Symbol: " + tokenSymbol);
                    res.locals.tokenSymbol = tokenSymbol;
                });
                token.name().then(function(tokenName) {
                    console.log("Token Name: " + tokenName);
                    res.locals.tokenName = tokenName;
                });
            });
            console.log("**** GET /getTokenBalance ****");
            truffle_connect.getTokenBalance(web3.eth.accounts[2], function (balance) {
                tokenBalance = web3.fromWei(balance, "ether").toString();
                console.log(tokenBalance);
                res.locals.tokenBalance = tokenBalance;
                if (res.locals.tokenBalance) {
                    res.render('./admin/account/account');
                } else {
                    res.render('./admin/categories/categories');
                }
            });*/
        } else {
            res.render('./admin/categories/categories');
        }

    } else {
        res.render('./admin/login/login');
    }

};