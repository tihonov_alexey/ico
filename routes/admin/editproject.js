var log = require('../../libs/log')(module);
var Projects = require('../../models/projects').Project;
var Categories = require('../../models/categories').Category;
var Tags = require('../../models/tags').Tags;
var fileupload = require('../../libs/fileupload');
var Config = require('../../libs/myconfig');

exports.get = function (req, res) {

    if (req.session.user && ((res.locals.adminrights == 'superadmin') || (res.locals.adminrights == 'startup'))) {

        res.locals.currentcat = req.params.id;
        res.locals.parent = 'me';
        res.locals.page = 'editproject';

        Categories.find({ismain: false}, function (err, categories) {
            if (categories && (categories.length > 0)) {

                res.locals.categories = categories;

                if (req.params.label != 'new') {

                    Projects.findOne({_id: req.params.label}, function (err, project) {
                        if (project) {

                            res.locals.title = 'Редактор: ' + project.title;
                            res.locals.project = project;
                            res.render('./admin/editproject/editproject');

                        } else {
                            res.locals.title = '404 Ничего не найдено';
                            res.status(404).render('./client/error/error', {
                                errorCode: 404,
                                errorText: 'Страница не найдена'
                            });
                        }
                    })
                } else {
                    res.locals.project = 'new';
                    res.locals.title = 'Новый проект';
                    res.render('./admin/editproject/editproject');
                }
            } else {
                res.locals.title = '404 Ничего не найдено';
                res.status(404).render('./client/error/error', {errorCode: 404, errorText: 'Страница не найдена'});
            }
        });

    } else {
        res.render('./admin/login/login');
    }
};

exports.post = function (req, res) {

    function checkTags(callback) {

        if (req.body.tags && (req.body.tags.length > 0)) {

            var counter = 0;
            for (var i = 0; i < req.body.tags.length; i++) {
                Tags.createTags(req.body.tags[i], function () {
                    counter++;
                    if (counter == req.body.tags.length) {
                        callback();
                    }
                })
            }

        } else {
            callback();
        }
    }

    if (req.body.action == 'newproject') {

        Projects.findOne({alias: req.body.alias}, function (err, project) {

            if (project) {

                res.send("Проект с таким URL уже существует!");

            } else {
                Projects.createProject(req.body.title, req.body.description, req.body.alias, req.body.categories, req.body.tags, req.body.moderator, req.body.days,function (err, product) {
                    if (err) {
                        res.send(err);
                    } else {
                        checkTags(function () {
                            res.send({id: product._id});
                        });
                    }
                });
            }
        });

    } else if (req.body.action == 'editproject') {

        Projects.findOne({alias: req.body.alias}, function (err, project) {

            if (project && (project._id != req.body.id)) {

                res.send("Проект с таким URL уже существует!");

            } else {

                Projects.editProject(req.body.id, req.body.title, req.body.description, req.body.alias, req.body.categories, req.body.tags, req.body.moderator, function (err, project) {

                    if (err) {
                        res.send(err);
                    } else {

                        checkTags(function () {
                            res.send({id: req.body.id});
                        });
                    }
                });
            }
        });

    } else if (req.body.action == 'savemainimage') {

        if (req.files.length && (req.files.length > 0)) {

            var dir = 'blog/' + req.body.id;

            // Можно сделать saveFile, если компрессия не нужна
            fileupload.saveWithResizeFile(dir, req.files[0], 'mainimage', 860, function (err, filelink) {
                // fileupload.saveFile(dir, req.files[0], 'mainimage', function(err, filelink){
                if (err) {
                    res.send(err);
                } else {

                    Projects.saveMainImage(req.body.id, filelink, function (err) {

                        if (err) {
                            res.send(err);
                        } else {
                            fileupload.slideCrop('..' + filelink, 385, 205, function (err) {
                                if (err) {
                                    res.send('Невозможно создать превью!');
                                } else {

                                    fileupload.sqCrop('..' + filelink, 300, function (err) {
                                        if (err) {
                                            res.send('Невозможно создать превью!');
                                        } else {
                                            res.send('Success');
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            res.send('Не выбран файл для загрузки!');
        }

    } else if (req.body.action == 'deletemainimg') {

        var filename = req.body.file;
        var ext = filename.substr(filename.lastIndexOf('.'));
        filename = filename.substr(0, filename.lastIndexOf('.'));
        var minfile = filename + '-min' + ext;
        var slidefile = filename + '-slide' + ext;

        fileupload.fileUnlink(req.body.file, function (err) {

            if (err) {
                res.send('Невозможно удалить файл!');
            } else {

                fileupload.fileUnlink(minfile, function (err) {
                    if (err) {
                        res.send('Невозможно удалить файл!');
                    } else {

                        fileupload.fileUnlink(slidefile, function (err) {
                            if (err) {
                                res.send('Невозможно удалить файл!');
                            } else {

                                Projects.saveMainImage(req.body.id, '', function (err) {
                                    if (err) {
                                        res.send('Невозможно удалить файл!')
                                    } else {
                                        res.send({ok: "ok"});
                                    }

                                });
                            }
                        });
                    }
                });
            }
        })

    } else if (req.body.action == 'setpublished') {

        var flag = true;
        if (req.body.flag == 'no') {
            flag = false;
        }

        Projects.setPublished(req.body.id, flag, function (err) {
            if (err) {
                res.send(err);
            } else {
                res.send({ok: 'ok'});
            }
        })

    } else if (req.body.action == 'deleteproject') {

        function removeProduct(id, callback) {

            Projects.deleteProject(id, function (err) {
                if (err) {
                    callback(err);
                } else {
                    fileupload.removeFolder('../files/blog/' + id, function (err) {
                        if (err) {
                            log.error('------removeFolder error: ' + err);
                        }
                        callback();
                    })

                }
            })
        }

        Projects.findOne({_id: req.body.id}, function (err, product) {

            if (product) {
                Projects.find({parent: product._id}, function (err, products) {
                    if (products && (products.length > 0)) {

                        var counter = 0;
                        for (var i = 0; i < products.length; i++) {

                            removeProduct(products[i]._id, function () {

                                counter++;
                                if (counter == products.length) {

                                    removeProduct(product._id, function (err) {
                                        if (err) {
                                            res.send(err);
                                        } else {
                                            res.send({ok: "Ok"});
                                        }
                                    })
                                }
                            });
                        }

                    } else {

                        removeProduct(product._id, function (err) {
                            if (err) {
                                res.send(err);
                            } else {
                                res.send({ok: "Ok"});
                            }
                        });
                    }
                })

            } else {

                removeProduct(product._id, function (err) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.send({ok: "Ok"});
                    }
                });
            }
        })

    } else if (req.body.action == 'searchtag') {

        var searchRequestStart = '^' + req.body.key + '.*';
        var searchRequestMiddle = ' ' + req.body.key + '.*';
        var params = {};
        var searchCondition = [{
            name: {
                $regex: searchRequestStart,
                $options: 'im'
            }
        }, {name: {$regex: searchRequestMiddle, $options: 'im'}}];
        params.$or = searchCondition;

        Tags.find(params).limit(8).exec(function (err, tags) {

            if (tags && (tags.length > 0)) {
                res.send({result: tags});
            } else {
                res.send({result: []});
            }

        })

    }

};