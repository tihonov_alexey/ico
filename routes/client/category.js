
var log = require('../../libs/log')(module);
var async = require('async');
var Categories = require('../../models/categories').Category;
var Localizer = require('./project');
var Projects = require('./project');

exports.get = function(req, res){

    var isMain = false;

    var categorySearchParams = {};
    var projectSearchParams = {};

    if (req.params.alias){
        categorySearchParams.alias = req.params.alias;
    } else {
        categorySearchParams.ismain = true;
        isMain = true;
    }

    projectSearchParams.published = true;

    res.locals.showMore = false;
    res.locals.page = 'category';

    async.parallel([
        getCategory,
        getProjects
    ], function(err){
        if (err){
            log.info('------ Ошибка: ' + err);
            res.status = 404;
            res.locals.pagenoindex = 'yes';
            res.locals.metatitle = '404 Ничего не найдено';
            res.render('./client/error/error');
        } else {
            res.render('./client/category/category');
        }
    });

    function getCategory(callback){

        Categories.findOne(categorySearchParams, function(err, category){

            if (category && ((category.ismain && isMain) || (!category.ismain && !isMain))){
                setLocalCategory(category, category, function(){
                    callback(null);
                });

            } else {

                callback("Нет такой категории");
            }
        })
    }

    function setLocalCategory(category, parent, callback){

        res.locals.category = category;
        res.locals.metatitle = category.htmltitle;
        res.locals.metadescription = category.htmldescription;
        res.locals.metakeywords = category.htmlkeywords;

        if (!isMain){

            projectSearchParams.categories = parent._id.toString();

            Localizer.findProjects(projectSearchParams,null,function(projects, isMore, last, notFound){

                if (isMore){
                    res.locals.showMore = true;
                    res.locals.from = last;
                }
                res.locals.projects = projects;
                callback();
            });
        } else {
            callback();
        }
    }

    function getProjects(callback){

        if (isMain){
            Localizer.findProjects(projectSearchParams,null,function(projects, isMore, last, notFound){

                if (isMore){
                    res.locals.showMore = true;
                    res.locals.from = last;
                }
                res.locals.projects = projects;
                callback(null);
            });
        } else {
            callback(null);
        }
    }

};

exports.post = function(req, res){

    if (req.body.action == 'more'){

        var params = {};
        if (req.body.ismain != 'true'){
            params.categories = req.body.category;
        }
        var date = new Date(req.body.last);
        params.moderated = {$lt: date};
        params.published = true;

        Localizer.findProjects(params,null, true, function(projects, isMore, last, notFound){

            res.render('./client/modules/projectItemArray', {projects: projects}, function(err, html){
                var data = {};
                if (isMore){
                    data.last = last;
                }
                if (html){
                    data.html = html;
                } else {
                    data.html = '';
                }
                res.send(data);
            });
        });
    }
};
