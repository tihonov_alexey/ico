
var log = require('../../libs/log')(module);
var MyConfig = require('../../libs/myconfig');
var Projects = require('../../models/projects').Project;


exports.get = function(req, res){


    Projects.findOne({alias: req.params.alias}, function(err, project){

        if (project){
            parseProject(project, project.categories[0]);

        } else {
            res.status = 404;
            res.locals.pagenoindex = 'yes';
            res.locals.metatitle = '404 Ничего не найдено 123';
            res.render('./client/error/error');
        }
    });

    function parseProject(project, relCategory){

        Projects.addView(project._id, function(){

            res.locals.project = project;
            res.locals.metatitle = project.htmltitle;
            res.locals.metadescription = project.htmldescription;
            res.locals.metakeywords = project.htmlkeywords;

            Projects.find({_id: {$ne: project._id}, categories: relCategory})
                .sort({moderated: -1})
                .limit(MyConfig.limits.relProjects)
                .exec(function(err, relprojects){
                    res.locals.relprojects = relprojects;
                    res.render('./client/project/project');
                });
        });
    }
};

function localizeProjects(projects, mainProject, callback){

    var relArray = [];
    var counter = 0;

    for (var i=0; i<projects.length; i++){

        findLocal(projects[i], function(localProject){

            counter ++;

            if (localProject && (!mainProject ||
                (localProject._id.toString() != mainProject._id.toString()))){
                relArray.push(localProject);
            }
            if (counter == projects.length){

                callback(relArray);
            }
        })
    }
}

function findLocal(project, callback){
    callback(project);
}

exports.localize = function(projects, mainProject, callback){

    localizeProjects(projects, mainProject, true, function(localizedProjects){
        callback(localizedProjects);
    })
};

exports.findProjects = function(params, mainProject, callback){

    Projects
        .find(params)
        .sort({moderated: -1})
        .limit(MyConfig.limits.pageProjects)
        .exec(function(err, projects){

            var isMore = false;
            var last = 'none';
            var notFound = false;
            var projectArray = [];

            if (projects && (projects.length > 0)){

                if (projects.length == MyConfig.limits.pageProjects){

                    var lastDate = projects[projects.length - 1].moderated;
                    params.moderated = {$lt: lastDate};
                    Projects.findOne(params, function(err, nextProject){

                        if (nextProject){
                            isMore = true;
                            last = lastDate;
                        } else {
                            isMore = false;
                        }
                        getResLocals();
                    })
                } else {
                    getResLocals();
                }

                function getResLocals(){

                    localizeProjects(projects, mainProject,function(localizedProjects){
                        callback(localizedProjects, isMore, last, notFound);
                    });
                }

            } else {
                notFound = true;
                callback(projectArray, isMore, last, notFound);
            }
        });
};
