const icoCrowdsale = artifacts.require('./icoCrowdsale.sol');
const icoToken = artifacts.require('./icoToken.sol');

module.exports = function(deployer, network, accounts) {
    const openingTime = web3.eth.getBlock('latest').timestamp + 60; // 60 secs in the future
    const closingTime = openingTime + 86400 * 20; // 20 days
    const rate = new web3.BigNumber(1);
    const wallet = accounts[1];
/*    const cap = 200 * 1000000; // Хардкеп
    const goal = 100 * 1000000; // Софткеп*/

    return deployer
        .then(() => {
            return deployer.deploy(icoToken);
        })
        .then(() => {
            return deployer.deploy(
                icoCrowdsale,
                openingTime,
                closingTime,
                rate,
                wallet,
                icoToken.address
            );
        });
};