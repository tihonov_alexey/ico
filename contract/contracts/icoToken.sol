pragma solidity >=0.4.24 <0.6.0;

import '../../node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20Mintable.sol';

contract icoToken is ERC20Mintable {
    string public name = "ICO TOKEN";
    string public symbol = "icoTKN";
    uint8 public decimals = 18;
}
