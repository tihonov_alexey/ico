pragma solidity >=0.4.24 <0.6.0;

import './icoToken.sol';
import '../../node_modules/openzeppelin-solidity/contracts/crowdsale/emission/MintedCrowdsale.sol';
import '../../node_modules/openzeppelin-solidity/contracts/crowdsale/validation/TimedCrowdsale.sol';


contract icoCrowdsale is TimedCrowdsale, MintedCrowdsale {
    constructor
        (
            uint256 _openingTime,
            uint256 _closingTime,
            uint256 _rate,
            address _wallet,
            ERC20Mintable _token
        )
        public
        Crowdsale(_rate, _wallet, _token)
        TimedCrowdsale(_openingTime, _closingTime) {

        }
}
