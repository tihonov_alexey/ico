var log = require('../libs/log')(module);
var fs = require('fs');
var Web3 = require('web3');
var provider = new Web3.providers.HttpProvider("http://localhost:8545");
var t_source = fs.readFileSync("/home/tixon/PhpstormProjects/app/contract/build/contracts/icoToken.json");
var c_source = fs.readFileSync("/home/tixon/PhpstormProjects/app/contract/build/contracts/icoCrowdsale.json");

var mongoose = require('../libs/mongoose'),

    Schema = mongoose.Schema;

var schema = new Schema({
    title: {
        type: String
    },
    image: {
        type: String
    },
    description: {
        type: String
    },
    alias: {
        type: String
    },
    categories: {
        type: Array
    },
    tags: {
        type: Array
    },
    created: {
        type: Date,
        default: Date.now()
    },
    creator: {
        type: String
    },
    moderated: {
        type: Date
    },
    moderatedhistory: {
        type: Array
    },
    published: {
        type: Boolean,
        default: true
    },
    views: {
        type: Number,
        default: 0
    },
    token: {
        type: String
    },
    contract: {
        type: String
    }
});

schema.index(
    {moderated: -1, token: '', contract: ''}
);

schema.index(
    {title: 'text', description: 'text'},
    {weights: {title:10, description:5}}
);

schema.statics = {
    createProject: function(title, description, alias, categories, tags, creator, days, callback){

        // Deploy ICO Token instance
        function deployToken(account, callback) {
            tokenContract.new({
                data: token_bytecode,
                from: account,
                gas: 3000000
            }, (err, inst) => {
                if (err) {
                    console.log(err);
                    return;
                }
                if(inst.address) {
                    console.log('Token address: ' + inst.address);
                    callback(inst);
                }
            });
        }

        // Deploy ICO CrowdSale contract instance
        function deployContract(token, account, callback) {
            console.log("Deploying the ICO CrowdSale contract");
            const openingTime = web3.eth.getBlock('latest').timestamp + 60; // 60 secs in the future
            const closingTime = openingTime + 86400 * days;
            const rate = new web3.BigNumber(1);
            const wallet = account;

            crowdSaleContract.new(openingTime, closingTime, rate, wallet, token.address, {
                data: c_bytecode,
                from: account,
                gas: 3000000
            }, (err, inst) => {
                if (err) {
                    console.log(err);
                    return;
                }
                if (inst.address) {
                    console.log('CrowdSale Contract address: ' + inst.address);
                    token.addMinter(inst.address);
                    callback(inst);
                }
            });
        }
        var Projects = this;

        var date = new Date();

        var project = new Projects({
            title: title,
            description: description,
            alias: alias,
            categories: categories,
            tags: tags,
            creator: creator,
            moderated: date
        });

        web3 = new Web3(provider);
        if (web3.isConnected()) {
            web3.eth.defaultAccount = web3.eth.accounts[0];
            var icoToken = JSON.parse(t_source);
            var icoCrowdSale = JSON.parse(c_source);
            var token_abi = icoToken.abi;
            var c_abi = icoCrowdSale.abi;
            var token_bytecode = icoToken.bytecode;
            var c_bytecode = icoCrowdSale.bytecode;

            // Contract object
            var tokenContract = web3.eth.contract(token_abi);
            var crowdSaleContract = web3.eth.contract(c_abi);

            deployToken( web3.eth.defaultAccount, function(tokenInstance) {
                if (tokenInstance) {
                    project.token = tokenInstance.address;
                    console.log("success");
                    deployContract(tokenInstance, web3.eth.defaultAccount, function(contractInstance) {
                        project.contract = contractInstance.address;
                        project.save(function(err){
                            if (err) {
                                log.error("------ DB ERROR ----- " + err);
                                callback('Невозможно добавить проект');
                            } else {
                                callback (null, project);
                            }
                        });
                    });
                }
            });
        }
    },
    editProject: function(id, title, description, alias, categories, tags, moderator, callback){
        var Projects = this;
        var date = new Date();

        var setParams = {};
        setParams.title = title;
        setParams.description = description;
        setParams.alias = alias;
        setParams.categories = categories;
        setParams.tags = tags;
        setParams.moderated = date;

        Projects.update(
            {_id: id},
            {
                $set:setParams
            }, function(err, opt){
                if (opt){
                    var moderatedhistory = {};
                    moderatedhistory.moderator = moderator;
                    moderatedhistory.date = date;
                    Projects.update(
                        {_id: id},
                        {
                            $push: {moderatedhistory: moderatedhistory}
                        },
                        function(){
                            callback(null, opt);
                        }
                    )
                } else {
                    log.error("------ DB ERROR ----- " + err);
                    callback("Ошибка базы данных")
                }
            }
        )
    },
    saveMainImage: function(id, image, callback){

        var Projects = this;
        Projects.update(
            {_id: id},
            {
                $set: {image: image}
            }, function(err, opt){
                if (opt){
                    callback(null, opt);
                } else {
                    log.error("------ DB ERROR ----- " + err);
                    callback("Ошибка базы данных")
                }
            }
        )
    },
    setPublished: function(id, flag, callback){

        var Projects = this;
        var setParams = {};
        if (flag){
            setParams.published = true;
        } else {
            setParams.published = false;
        }

        Projects.update(
            {_id: id},
            {
                $set:setParams
            }, function(err, opt){
                if (opt){
                    callback(null, opt);
                } else {
                    log.error("------ DB ERROR ----- " + err);
                    callback("Ошибка базы данных")
                }
            }
        )
    },
    addView: function(id, callback){

        var Projects = this;

        Projects.update(
            {_id: id},
            {
                $inc: {views: 1}
            }, function(err, opt){
                if (opt){
                    callback(null, opt);
                } else {
                    log.error("------ DB ERROR ----- " + err);
                    callback("Ошибка базы данных")
                }
            }
        )

    },
    deleteProject: function(id, callback){
        var Projects = this;

        Projects.findOne({_id: id}, function(err, project){

            if (project){
                Projects.remove({parent: project._id}, function(){
                    Projects.remove({_id: project._id}, function(err){
                        if (err){
                            callback ("Невозможно удалить проект");
                        } else {
                            callback (null, id);
                        }
                    });
                });
            } else {
                callback ("Невозможно удалить проект");
            }
        });

    }

};

exports.Project = mongoose.model('Project', schema);